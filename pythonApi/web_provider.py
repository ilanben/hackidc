import urllib
import json

JSON_HERZELIA = 'http://api.openweathermap.org/data/2.5/forecast?id=294778&APPID=670bdf6a462b93cd61c96bf894917626'


def get():
    html = urllib.urlopen(JSON_HERZELIA).read()
    json_data = json.loads(html)['list'][0]

    dictionary = {'city': 'Herzliya',
                  'web_area_temperature': (json_data['main']['temp'] / 10),
                  'web_humidity': json_data['main']['humidity'],
                  'Web_weather': json_data['weather'][0]['description'],
                  'web_wind_speed': json_data['wind']['speed'],
                  'web_wind_direction': json_data['wind']['deg']}

    return dictionary
