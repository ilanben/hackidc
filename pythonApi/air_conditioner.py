import minimalmodbus
import serial

class ElectraAc( minimalmodbus.Instrument):
    """Instrument class for Electra display controller.

    Communicates via Modbus RTU protocol (via RS232 or RS485), using the *MinimalModbus* Python module.
    Args:
        * portname (str): port name
        * slaveaddress (int): slave address in the range 1 to 247
    Implemented with these function codes (in decimal):

    ==================  ====================
    Description         Modbus function code
    ==================  ====================
    Read registers      3
    Write registers     16
    ==================  ====================
    """

    def __init__(self, portname, slaveaddress):
        minimalmodbus.Instrument.__init__(self, portname, slaveaddress)
        self.serial.baudrate = 115200
        self.serial.parity = serial.PARITY_EVEN
        self.serial.stopbits = 1
        self.serial.timeout = 0.05

Instrument = None

def connect(com):
    global Instrument
    Instrument = ElectraAc(com, 1)

def set(fan, temp, mode):
    global Instrument

    #init msg
    msg = ((((fan << 2) << 5) + temp) << 3) + mode
    Instrument.write_register(0x3101, msg)
    #send
    Instrument.write_register(0x3100, 0b11)
    #Instrument.write_register(0x3105, 1)
