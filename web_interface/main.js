/**
 * Created by obama on 28/04/2017.
 */
var express = require('express');
var app = express();

app.use(express.static(__dirname + '/app'));
app.use('/api', express.static('../data/data.json'))
app.listen(3000);
