//    muRata FTN Thermistor Starter Kit          
//    Last updated : 17-Nov.2015               
//                                          
//     _____Pin Layout of starter kit______  
//    | VCC (5V)                   _____   | 
//    | OUT                        |_CN_|--> connect FTN Thermistor 
//    |_GND________________________________|
//                                          
//    Pin connection                                         
//    Starter Kit       arduino UNO board
//    OUT         <-->  A0
//
//    Thermistor is resistive component whose resistance value varies in respond to its temperature.
//    On the board, 5V is applied to thermistor and reference register (10kohm) and devided voltage is obtained at OUT.
//    Intaking OUT into ADC and applying approximation formula, we obtain temperature value.
//
//    Approximation formula for various items is available at 
//    SimSurfing : http://ds.murata.co.jp/software/simsurfing/en-us/
//    For detail, please refer to application note.

double vcc  = 5.0;
int tempInsidePort = 0;
int tempOutsidePort = 1;
int fanPort = 2;
int windowPort = 4;
double tempInside = 0;
double tempOutside = 0;
boolean fan  = false;
boolean window = false;

String signal;


void setup() {
  Serial.begin(9600);
  Serial.setTimeout(50);
  
  
  pinMode(fanPort, OUTPUT);
  pinMode(windowPort, OUTPUT);

  // Configure ADC
  analogReference(DEFAULT);
}

void loop() {
  tempInside = getTemp(tempInsidePort);
  Serial.print("temp_inside:float:");
  Serial.println(tempInside);

  tempOutside = getTemp(tempOutsidePort);
  Serial.print("temp_outside:float:");
  Serial.println(tempOutside);

  if(Serial.available() > 0){
    signal = Serial.readString();
    if(fan && signal.equals("fan0")){
      fan = false;
      digitalWrite(fanPort, LOW);
      Serial.println("fan:bool:0");
    } else if(!fan & signal.equals("fan1")){
      fan = true;
      digitalWrite(fanPort, HIGH);
      Serial.println("fan:bool:1");
    }
    
    if(window && signal.equals("window0")){
      window = false;
      digitalWrite(windowPort, LOW);
      Serial.println("window:bool:0");
    } else if(!window & signal.equals("window1")){
      window = true;
      digitalWrite(windowPort, HIGH);
      Serial.println("window:bool:1");
    }
    }
    
  delay(1000);
}


double getTemp(int port){
  double temp_read   = analogRead(port);
  double temp_vol     = ((double) temp_read / 1023 ) * vcc; 
  double temp    = -0.30779 * pow(temp_vol, 5) + 4.1545 * pow(temp_vol,4) - 23.272 * pow(temp_vol,3) + 68.015 * pow(temp_vol,2) - 126.35 * temp_vol + 160.42;

  return temp;
}

