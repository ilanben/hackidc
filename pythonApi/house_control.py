import time
import house
import air_conditioner
import json
import web_provider

timer_sleep = 5
user_perfect_temperature = 24

curr_air_cond_temp = -1
fan_speed = 1

ventilator=0
window =0

print("This program has stated on "+time.ctime())

air_conditioner.connect('/dev/ttyUSB0')
house.start_refreshing('/dev/ttyACM0')
#air_conditioner.set(2, 22, 2)

file = open('../data/data.db', 'w')

while True:
    houseTemp = house.get_temp_inside()
    if (not houseTemp) or (not house.get_temp_outside()):
        continue

    houseTemp = int(houseTemp)

    delta = abs(houseTemp - user_perfect_temperature)
    if houseTemp == user_perfect_temperature:
        mod = 0
        if curr_air_cond_temp != -2:
            curr_air_cond_temp = -2
            air_conditioner.set(0, 0, 0)
        continue
    elif houseTemp > user_perfect_temperature:
        mod = 1
        temp = houseTemp - delta * 2
    else:
        mod = 2
        temp = houseTemp + delta * 2

    if abs(temp-house.get_temp_outside()) < 3:
        if window ==0:
            house.set_window(True)
            window = 1
    else:
        if window == 1:
            house.set_window(False)
            window =0

    if houseTemp-user_perfect_temperature > 2:
        if ventilator == 0:
            house.set_fan(True)
            ventilator = 1
    else:
        if ventilator == 1:
            house.set_fan(False)
            ventilator = 0

    air_conditioner.set(int(fan_speed), int(min(temp, 31)), int(mod))
    curr_air_cond_temp = temp

    data = house.sensors
    data.update(web_provider.get())
    data.update({'desired_temperature': user_perfect_temperature})
    json.dump(data, open('../data/data.json', 'w'))

    time.sleep(timer_sleep)

    file.write(str(time.ctime()) + ' ' + str(houseTemp) + ' ' + str(temp) + ' ' + str(window) + '\n')
