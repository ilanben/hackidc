import thread
import util
import serial
import time

sensors = {}
serial_connection = None
refresh_thread = None
sending_thread = None
is_refresh_running = False
is_sending = False
send_queue = []

'''
	refresh_rate must b
'''


def start_refreshing(location, refresh_rate=1, sending_rate=1):
    global serial_connection, refresh_thread, is_refresh_running, is_sending, sending_thread
    if not serial_connection:
        serial_connection = serial.Serial(location, 9600)
        is_refresh_running = True
        is_sending = True
        refresh_thread = thread.start_new_thread(refresh, (refresh_rate,))
        sending_thread = thread.start_new_thread(send_in_background, (sending_rate,))


def send_in_background(sending_rate):
    while is_sending:
        if len(send_queue):
            packet = send_queue.pop(0)
            serial_connection.write(packet)
        time.sleep(sending_rate)

def __send__(packet):
    send_queue.append(packet)


def refresh(refresh_rate):
    import time
    time.sleep(1)
    while is_refresh_running:

        line = serial_connection.readline().strip()
        params = line.split(":")
        if len(params) == 3:
            val = params[2]
            val_type = params[1]
            name = params[0]
            if val_type == 'float':
                val = float(val)
            elif val_type == 'str':
                val = str(val)
            elif val_type == 'bool':
                val = True if val == '1' else False
            else:
                # in this situation, the value can't be parsed, so we skip it
                continue
            sensors[name] = val
        time.sleep(refresh_rate)
    serial_connection.close()


def set_fan(status):
    s = ('1' if status else '0')
    __send__("fan" + s)


def set_window(status):
    s = ('1' if status else '0')
    __send__("window" + s)


def stop_refreshing():
    global is_refresh_running, is_sending
    is_sending = False
    is_refresh_running = False



def get(key, default=None):
    return sensors[key] if sensors.has_key(key) else default


def get_temp_inside(default=None):
    return get('temp_inside', default)


def get_temp_outside(default=None):
    return get('temp_outside', default)


def get_fan(default=None):
    return get('fan', False)


def get_window(default=None):
    return get('window', False)
